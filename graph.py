from dash import dash, dcc, html,Input, Output ,State,register_page,callback
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas
import plotly.graph_objects as go
from pycaret.classification import *
import numpy as np 
model = load_model('data\Predict_Retire_Model')
register_page(__name__, path='/')
#app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
data = pandas.read_excel('data_dropout_59-64.xlsx')
data['DEGREE_ID'] = data["DEGREE_ID"].dropna()
data['FUND_NAME_CODE'] = data["FUND_NAME_CODE"].replace('Y',1)
data['FUND_NAME_CODE'] = data["FUND_NAME_CODE"].replace('N',2)
data['FUND_NAME_CODE'] = data["FUND_NAME_CODE"].replace('other',3)
data['SEX_SHORT_NAME_THAI'] = data['SEX_SHORT_NAME_THAI'].replace('ช',0)
data['SEX_SHORT_NAME_THAI'] = data['SEX_SHORT_NAME_THAI'].replace('ญ',1)
# data['ENT_METHOD'] = data['ENT_METHOD'].replace('B1',103)
# data['ENT_METHOD'] = data['ENT_METHOD'].replace('D0',100)
# data['ENT_METHOD'] = data['ENT_METHOD'].replace('P4',101)
# data['ENT_METHOD'] = data['ENT_METHOD'].replace('E1',102)
data["IN_PROVINCE"] = data["IN_PROVINCE"].replace(["N","Y"],[0,1])
data["IN_PROVINCE_CAMPUS"] = data["IN_PROVINCE_CAMPUS"].replace(["N","Y"],[0,1])


first_card = dbc.Card(
    dbc.CardBody(
        [
            html.Div(
                [
                    html.Div(
                        [
                            html.H5("สาขาวิชา"),
                            dcc.Dropdown(
                                options=[{"label": str(p), "value": int(p)} for p in data["MAJOR_ID"].dropna().unique()]
                                        ,
                                id='input9',
                                searchable=False,
                                className="row-1 card",
                                style={
                                    "width": "11vw",
                                    "height": "5vh",
                                },
                            ),
                        ],
                        style={"display": "inline-block", "margin-right": "20px","margin-left": "20px"},
                    ),
                    html.Div(
                        [
                            html.H5("ภาควิชา"),
                            dcc.Dropdown(
                                options=
                                        [{"label": str(p), "value": int(p)} for p in data["DEPT_ID"].dropna().unique()],
                                id = 'input10',
                                searchable=False,
                                className="row-1 card",
                                style={
                                    "width": "11vw",
                                    "height": "5vh",
                                },
                            ),
                        ],
                        style={"display": "inline-block","margin-right": "20px"},
                    ),
                    html.Div(
                        [
                            html.H5("ประเภทการศึกษา"),
                            dcc.Dropdown(
                                 options=
                                        [{"label": str(p), "value": int(p)} for p in data["STUDY_TYPE_ID"].dropna().unique()],
                                id='input11',
                                searchable=False,
                                className="row-1 card",
                                style={
                                    "width": "11vw",
                                    "height": "5vh",
                                },
                            ),
                        ],
                        style={"display": "inline-block","margin-right": "20px","margin-top": "20px"},
                    ),
                    html.Div(
                        
                            [
                                html.H5("ปริญญาบัตรที่จบ"),
                                dcc.Dropdown(
                                    options=[{"label": str(p), "value": float(p)} for p in data["DEGREE_ID"].dropna().unique()],
                                    id="input12",
                                    searchable=False,
                                    className="row-1 card",
                                    style={"width": "11vw", "height": "5vh"},
                                ),
                            ],
                            style={"display": "inline-block", "margin-right": "20px"},
                        ),
                    html.Div(
                        [
                            html.H5("เพศ"),
                            dcc.Dropdown(
                                options=[
                                    dict(label=p.strip(), value=p.strip())
                                    for p in ['0','1']],
                                id='input13',
                                searchable=False,
                                className="row-1 card",
                                style={
                                    "width": "11vw",
                                    "height": "5vh",
                                }
                            ),
                        ],
                        style={"display": "inline-block","margin-right": "20px"}
                    ),
                    # html.Div(
                    #     [
                    #         html.H5("โครงการที่เข้าศึกษา"),
                    #         dcc.Dropdown(
                    #             options=[dict(label=p, value=p)
                    #                     for p in data["ENT_METHOD"].unique()],
                    #             id='input14',
                    #             searchable=False,
                    #             className="row-1 card",
                    #             style={
                    #                 "width": "11vw",
                    #                 "height": "5vh",
                    #             }
                    #         ),
                    #     ],
                    #     style={"display": "inline-block","margin-right": "20px","margin-left": "20px","margin-top": "20px"}
                    # ),
                    html.Div(
                        [
                            html.H5("IN_PROVINCE"),
                            dcc.Dropdown(
                                options=[dict(label=p.strip(), value=p.strip())
                                    for p in ['0','1']],
                                id = 'input15',
                                searchable=False,
                                className="row-1 card",
                                style={
                                    "width": "11vw",
                                    "height": "5vh",
                                }
                            ),
                        ],
                        style={"display": "inline-block","margin-right": "20px"}
                    ),
                    html.Div(
                        [
                            html.H5("IN_PROVINCE_CAMPUS"),
                            dcc.Dropdown(
                                options=[dict(label=p.strip(), value=p.strip())
                                    for p in ['0','1']],
                                id='input16',
                                searchable=False,
                                className="row-1 card",
                                style={
                                    "width": "11vw",
                                    "height": "5vh",
                                }
                            ),
                        ],
                        style={"display": "inline-block","margin-right": "20px"}
                    ),
                    html.Div(
                        [
                            html.H5("ได้รับทุน"),
                            dcc.Dropdown(
                                options=[dict(label=p.strip(), value=p.strip())
                                    for p in ['1','2','3']],
                                id='input17',
                                searchable=False,
                                className="row-1 card",
                                style={
                                    "width": "11vw",
                                    "height": "5vh",
                                }
                            ),
                        ],
                        style={"display": "inline-block","margin-right": "20px"}
                    ),
                    html.Div(
                        [
                            html.H5("สถานะทางครอบครัว"),
                            dcc.Dropdown(
                                options=[dict(label=p, value=p)
                                        for p in data["PARENTS_MARRIED_NAME_CODE"].unique()],
                                id = 'input18',
                                searchable=False,
                                className="row-1 card",
                                style={
                                    "width": "11vw",
                                    "height": "5vh",
                                }
                            ),
                        ],
                        style={"display": "inline-block","margin-right": "20px"}
                    ),
                ],
            ),
                   html.Div([
                        html.Br(),
                            dcc.Input(id="input1", type="text", placeholder="เกรดปี1เทอม1", 
                                    style={'marginRight': '10px',"margin-left": "150px",'marginTop': '10px','border-radius':'5px','marginleft': '320px',"text-align":"center"}),
                            dcc.Input(id="input2", type="text", placeholder='เกรดปี1เทอม2', 
                                    style={'marginRight': '10px','border-radius':'5px',"text-align":"center"}),
                            dcc.Input(id="input3", type="text", placeholder='เกรดปี2เทอม1',  
                                    style={'marginRight': '10px','marginTop': '10px','border-radius':'5px',"text-align":"center"}),
                            dcc.Input(id="input4", type="text", placeholder='เกรดปี2เทอม2', 
                                    style={'marginRight': '160px','border-radius':'5px',"text-align":"center"}),
                        html.Br(),
                            dcc.Input(id="input5", type="text", placeholder='เกรดปี3เทอม1', 
                                    style={'marginRight': '10px',"margin-left": "150px",'marginTop': '10px','border-radius':'5px','marginleft': '320px',"text-align":"center"}),
                            dcc.Input(id="input6", type="text", placeholder='เกรดปี3เทอม2', 
                                    style={'marginRight': '10px','border-radius':'5px',"text-align":"center"}),
                            dcc.Input(id="input7", type="text", placeholder='เกรดปี4เทอม1', 
                                    style={'marginRight': '10px','marginTop': '10px','border-radius':'5px',"border-color":"#ccccc","text-align":"center"}),
                            dcc.Input(id="input8", type="text", placeholder='เกรดปี4เทอม2',style={'marginRight': '10px','marginTop': '10px','border-radius':'5px',"text-align":"center"}, debounce=True),
                            html.Div(id="output"),
                    ]
                ),
                dbc.Button("Predictions", color="dark gray",id="compute-button",n_clicks=0,
                           style={'marginLeft': '440px','marginTop': '18px',"width": "11vw", "height": "5vh"}),
        ]
    ),
    style={
        # "background-color": "#F7F6CF",
        "background-color": "#FAF7F0",    #card bg
        "margin-bottom": "10px",
        "width": "65vw",
        "height": "65vh",
        "position": "center",
        "display": "flex",
        "box-shadow": "0 4px 8px 0 rgba(0,0,0,0.1)",
        # "margin-left": "0.25vw",
        "margin-bottom": "20px",
    }
)

second_card = dbc.Card(
    dbc.CardBody(
    
        [
            html.H5("Prediction", className="card-title"),
            html.P(""
            ),
            html.Div(id='prediction'),
      
        ]
    ),
    style={
        # "margin-bottom": "10px",
        "background-color": "#FAF7F0",
        "width": "30vw",
        "height": "65vh",
        "position": "center",
        "display": "flex",
        "box-shadow": "0 4px 8px 0 rgba(0,0,0,0.1)",
        "margin-left": "33.5vw",
        "margin-bottom": "20px",
        "font-family": "Arial, sans-serif", "font-size": "64px", "color": "#E50914", "text-shadow": "2px 2px #000", "letter-spacing": "2px", "animation": "neon 1.5s ease-in-out infinite alternate"})
    

third_card = dbc.Card(
    dbc.CardBody(
        [
            html.H5("3", className="card-title"),
            html.P(""),
        ]
    ),
    style={
        "background-color": "#FAF7F0",
        "margin-bottom": "10px",
        "width": "33vw",
        "height": "65vh",
        "position": "center",
        "display": "flex",
        "box-shadow": "0 4px 8px 0 rgba(0,0,0,0.1)",
        # "margin-left": "0.25vw",
        "margin-bottom": "20px",
    }
)

pie_data = [go.Pie(labels=['สำเร็จการศึกษา', 'ลาออก', 'ตกออก', 'ลาพักการศึกษา', 'เสียชีวิต', 'กำลังศึกษา'], values=[40,10,10,20,10,10],pull=0.05,hole=0.4)]
pie_layout = go.Layout(title='Donut Chart',paper_bgcolor = '#FAF7F0',plot_bgcolor = '#FAF7F0')
pie_graph = dcc.Graph(id='pie-graph', figure={'data': pie_data, 'layout': pie_layout},
                        style={'height': '50vh'})

# create the scatter graph
scatter_data = [go.Scatter(x=[1, 2, 3], y=[4, 5, 6], mode='markers')]
scatter_layout = go.Layout(title='Scatter Chart',paper_bgcolor = '#FAF7F0',plot_bgcolor = '#FAF7F0')
scatter_graph = dcc.Graph(id='scatter-graph', figure={'data': scatter_data, 'layout': scatter_layout},
                        style={'height': '50vh'})

# add the graphs to the card body
fourth_card = dbc.Card(
    dbc.CardBody(
        [
            html.H5("Data Statistics", className="card-title",style={"text-align": "center"}),
            dbc.Row(
                [
                    dbc.Col(pie_graph, width=6),
                    dbc.Col(scatter_graph, width=6),
                ]
            )
        ]
    ),
    style={
        "background-color": "#FAF7F0",
        "width": "62vw",
        "height": "65vh",
        "position": "center",
        "display": "flex",
        "box-shadow": "0 4px 8px 0 rgba(0,0,0,0.1)",
        "margin-left": "1.5vw",
        "margin-bottom": "20px",
    }
)

cards = dbc.Row(
    [
        dbc.Col(first_card, width=4),
        dbc.Col(second_card, width=6,),
        dbc.Col(third_card, width=4),
        dbc.Col(fourth_card, width=6),
    ],
    style={'marginTop': '20px'}
)


layout = html.Div(
    children=
        cards,
        style={'margin': '20px'},
    )

@callback(
    Output("output", "children"),
    Input("compute-button", "n_clicks"),
    State("input1", "value"),
    State("input2", "value"),
    State("input3", "value"),
    State("input4", "value"),
    State("input5", "value"),
    State("input6", "value"),
    State("input7", "value"),
    State("input8", "value"),
    State("input9", "value"),
    State("input10", "value"),
    State("input11", "value"),
    State("input12", "value"),
    State("input13", "value"),
    # State("input14", "value"),
    State("input15", "value"),
    State("input16", "value"),
    State("input17", "value"),
    State("input18", "value"),
)
def update_output(n_clicks, input1, input2, input3, input4, input5, input6, input7, input8, input9,
                  input10, input11, input12, input13,  input15, input16, input17, input18):
    if any(val is None for val in [input9, input10, input11, input12, input13,  input15, input16, input17, input18]):
        return "   Please fill in all input fields." 
                                                         
    Data_Test = pandas.DataFrame({'MAJOR_ID':input9, 
                        'DEPT_ID':input10,  
                        'STUDY_TYPE_ID':input11,
                        'DEGREE_ID':input12, 
                        'SEX_SHORT_NAME_THAI':input13,
                        # 'ENT_METHOD':input14,  
                        'IN_PROVINCE':input15,
                        'IN_PROVINCE_CAMPUS':input16,
                        'เกรดปี1เทอม1':input1,
                        'เกรดปี1เทอม2':input2,
                        'เกรดปี2เทอม1':input3,
                        'เกรดปี2เทอม2':input4,
                        'เกรดปี3เทอม1':input5,
                        'เกรดปี3เทอม2':input6,                                                
                        'เกรดปี4เทอม1':input7,
                        'เกรดปี4เทอม2':input8,
                        'FUND_NAME_CODE':input17,
                        'PARENTS_MARRIED_NAME_CODE':input18},index=[0])
    Data_Test[['เกรดปี1เทอม1','เกรดปี1เทอม2',
                     'เกรดปี2เทอม1','เกรดปี2เทอม2',
                     'เกรดปี3เทอม1','เกรดปี3เทอม2',
                     'เกรดปี4เทอม1','เกรดปี4เทอม2',]]= Data_Test[['เกรดปี1เทอม1','เกรดปี1เทอม2','เกรดปี2เทอม1',
                                                                'เกรดปี2เทอม2','เกรดปี3เทอม1','เกรดปี3เทอม2',
                                                                'เกรดปี4เทอม1','เกรดปี4เทอม2',]].apply(lambda row: row.fillna(row.median()), axis=1)
    Data_Test = Data_Test.astype({
    'MAJOR_ID': int,
    'DEPT_ID': int,
    'STUDY_TYPE_ID': int,
    'DEGREE_ID': float,
    'SEX_SHORT_NAME_THAI':str,
    # 'ENT_METHOD':str,
    'IN_PROVINCE':str, 
    'IN_PROVINCE_CAMPUS':str,
    'เกรดปี1เทอม1': float,
    'เกรดปี1เทอม2': float,
    'เกรดปี2เทอม1': float,
    'เกรดปี2เทอม2': float,
    'เกรดปี3เทอม1': float,
    'เกรดปี3เทอม2': float,
    'เกรดปี4เทอม1': float,
    'เกรดปี4เทอม2': float,
    'FUND_NAME_CODE':str,
    'PARENTS_MARRIED_NAME_CODE': int
})
    Dict = {0:"G",1:"R"}
    prediction = model.predict_proba(Data_Test)[0]
    status,percent = Dict[(prediction.tolist()).index(prediction.max())],f"{round(prediction.max()*100,2)}"
    
    if status == 'G':
        status = 'R'
        percent = f"{(100 - float(percent)):.2f}"
        return (status, percent)
    else :
        return (status, percent)
    
@callback(
    Output("prediction", "children"),
    Input("output", "children")
)
def show_prediction(output_value):
    status, percent = output_value[:2]
    # if float(percent) > 50:
    return html.Div([
        html.H4(f"{status}", style={"font-family": "Arial, sans-serif", "font-size": "64px", "color": "#E50914", "text-shadow": "2px 2px #000", "letter-spacing": "2px", "animation": "neon 1.5s ease-in-out infinite alternate"}),
        html.H4(f"{percent}", style={"font-family": "Arial, sans-serif", "font-size": "32px", "color": "#E50914", "text-shadow": "2px 2px #000", "letter-spacing": "2px", "animation": "neon 1.5s ease-in-out infinite alternate"})
    ])
    # else :
    #     return html.Div([
    #     html.H4(f"{status}", style={"font-family": "Arial, sans-serif", "font-size": "64px", "color": "#00FF00", "text-shadow": "2px 2px #000", "letter-spacing": "2px", "animation": "neon 1.5s ease-in-out infinite alternate"}),
    #     html.H4(f"{percent}", style={"font-family": "Arial, sans-serif", "font-size": "32px", "color": "#00FF00", "text-shadow": "2px 2px #000", "letter-spacing": "2px", "animation": "neon 1.5s ease-in-out infinite alternate"})
    # ])
# if __name__ == "__main__":
#     app.run_server(debug=True)
    